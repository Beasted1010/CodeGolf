	EXPORT main
	INCLUDE	LM4F120H5QR.inc
	AREA K,CODE
		; Code Golf : Input # -> ( # Neg ? (Output = -1) : (# !Zero ? Output = 1 : Output = 0) )
main
 PUSH{LR}
A MOV R1,#1
 B C
Ba MOV R1,#-1
C BL GetCh
 CMP R0,#'-'
 BEQ Ba
 CMP R0,#'0'
 BLO A
 CMP R0,#'9'
 BHI A
 MOV R2,#0
 MOV R3,#10
D SUB R0,#'0'
 MLA R2,R3,R2,R0
 BL GetCh
 CMP R0,#'0'
 BLO E
 CMP R0,#'9'
 BLS D
E MUL R0,R1,R2
 MOVS R0,R0,LSL #1
 BEQ F
 MOVCS R0,#'-'
 BLCS UART_OutChar
 MOV R0,#'1'
 B G
F MOV R0,#'0'
G BL UART_OutChar
 POP{PC}
GetCh PUSH{LR}
 BL UART_InChar
 BL UART_OutChar
 CMP R0,#0x0D
 POPNE{PC}
 MOV R0,#0x0A
 BL UART_OutChar
 MOV R0,#0x0D
 POP{PC}
 END