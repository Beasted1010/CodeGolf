#include "stdio.h"
#include "stdlib.h"
#include "math.h"

#define LogError(arg) printf(arg)

int SumOfMultiples( int max_number, int number_list[], int list_length );

int main()
{
    printf("Enter in a list of numbers to be used in calculating the sum of their mulitples\n");
    printf("A max of 10.\nEnter in 0 to end the list (0 is not included)\n");
    int list_length = 0;
    int i;

    int *temp;
    // TODO: This may be causing free to fail?? whooo knows
    temp = malloc( sizeof(int) * 10 );

    int value;

    for( i = 0; i < 10; i++ )
    {
        scanf("%d", &value);
        if( value == 0 ) { break; }
        temp[i] = value;
        list_length++;
    }
    //printf("HERE  1\n");
    int *number_list;
    number_list = malloc( list_length );
    for( i = 0; i < list_length; i++ )
    {
        number_list[i] = temp[i];
    }

    //printf("HERE  2\n");
    // TODO: The below crashes if included when entering in 2,3,5,0 above
    //free( temp );

    //printf("HERE  3\n\n");

    printf("Enter in the max number to find multiples up to: ");
    int max_number;
    scanf("%d", &max_number);

    printf("\n\nThe sum of the multiples is: %d\n", SumOfMultiples( max_number, number_list, list_length ));

    return 0;
}

// TODO: FIX :(
int SumOfMultiples( int max_number, int number_list[], int list_length )
{
    int i, j, k;
    //TODO: We can do better than creating space for EVERy multiple... sqrt(max_number)? thats for factors.. but...?
    int multiples[max_number];
    int multiple, num_multiples = 0;

    int sum = 0;
    // Pinpoint each multiple that is less than the sqrt(of our number)
    //TODO: sqrt of our number, is this appropriate?
    for( i = 0, j = 0, k = 0; i < list_length; i++, j = 0 )
    {
        while( j < max_number )
        {
            multiple = number_list[i] * (j + 1);
            j++;
            if( multiple < max_number )
            {
                multiples[k] = multiple;
                k++;
                num_multiples++;
            }
        }
    }
    int counter = 0;

    for( i = 0; i < num_multiples; i++ )
    {
        for( j = 0; j < num_multiples; j++ )
        {
            if( multiples[i] == multiples[j] && i != j )
            {
                multiples[i] = 0;
                counter++;
            }
        }
    }

    // Add up all the multiples
    for( i = 0; i < num_multiples; i++ )
    {
        if( multiples[i] != 0 )
        { sum += multiples[i]; }
    }
    return sum;
}
