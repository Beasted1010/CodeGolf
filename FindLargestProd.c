






#include "stdio.h"
#include "stdlib.h"

int FindLargestProd( int seq[], int length );
int FindMax( int seq[], int length );
int FindMin( int seq[], int length );

/*
TODO:
Does not work for consecutive same values
exp: [2, 2, 2] fails due to 2,2,2
exp: [3, 3, 8, 9, 1, 7, 7, 2, 2, 4] fails due to 3,3 7,7 and/or 2,2
*/

int main()
{
    printf("Enter in the length of the Array: ");
    int length = 0;
    scanf( "%d", &length );

    int* seq = malloc( length * sizeof( int ) );
    int i;
    printf("Enter in the values of the %d element array\n", length);
    for( i = 0; i < length; i++ )
    {
        scanf( "%d", &seq[i] );
    }
    int largest_product = FindLargestProd( seq, length );

    printf("The largest subsequence product is: %d\n", largest_product);

    return 0;
}

int FindLargestProd( int seq[], int length )
{
    int i, max = FindMax( seq, length );
    int min = FindMin( seq, length );
    int counter, counter1 = 1, counter2 = 1;
    // Keeping track of the index for efficiency reasons (although insignificant)
    int max_index = 0, min_index = 0;
    int save, another_subseq = 0;
    int two_products = 0;
    int second_subseq_index;

    for( i = 0; ; i++ )
    {
        // Count the integers in the range [min, max] (including bounds)
        if( seq[i] == max )
        {
            max_index = i;
            do{ counter1++; i++; }while( seq[i] != min );
            save = i;
            do
            {
                if( seq[i] == max )
                {
                    another_subseq = 1;
                    second_subseq_index = save;
                }
                i++;
            }while( i < length || !another_subseq );
            if( another_subseq )
            {
                i = save;
                do{ counter2++; i++; }while( seq[i] != max );
            }
            if( counter1 == counter2 )
            {
                counter = counter1;
                two_products = 1;
            }
            else
            {
                if( counter1 > counter2 )
                { counter = counter1; }
                else
                { counter = counter2; max_index = second_subseq_index; }
            }
            break;
        }
        else if( seq[i] == min )
        {
            min_index = i;
            do{ counter1++; i++; }while( seq[i] != max );
            save = i;
            do
            {
                if( seq[i] == min )
                {
                    another_subseq = 1;
                    second_subseq_index = save;
                }
                i++;
            }while( i < length || !another_subseq );
            if( another_subseq )
            {
                i = save;
                do{ counter2++; i++; }while( seq[i] != min );
            }
            if( counter1 == counter2 )
            {
                counter = counter1;
                two_products = 1;
            }
            else
            {
                if( counter1 > counter2 )
                { counter = counter1; }
                else
                { counter = counter2; max_index = second_subseq_index; }
            }
            break;
        }
    }
    // Either min_index or max_index will be zero
    int first_product, second_product = 1, product, stopping_point = min_index + max_index + counter;
    int starting_point = min_index + max_index;
    first_product = seq[starting_point];

    // i starts at the starting index and goes to the stopping index
    for( i = starting_point; i < stopping_point; i++ )
    {
        if( i + 1 != stopping_point )
        { first_product *= seq[i + 1]; }
    }
    if( two_products )
    {
        starting_point = second_subseq_index;
        second_product = seq[starting_point];
        stopping_point = starting_point + counter;
        for( i = starting_point; i < stopping_point; i++ )
        {
            if( i + 1 != stopping_point )
            { second_product *= seq[i + 1]; }
        }
        printf("TWO PRODUCTS\n");
    }
    first_product > second_product ? (product = first_product) : (product = second_product);

    return product;
}


int FindMax( int seq[], int length )
{
    int i, max = seq[0];

    for( i = 0; i < length; i++ )
    {
        if( seq[i] > max )
        { max = seq[i]; }
    }

    return max;
}

int FindMin( int seq[], int length)
{
    int i, min = seq[0];

    for( i = 0; i < length; i++ )
    {
        if( seq[i] < min )
        { min = seq[i]; }
    }
    return min;
}
